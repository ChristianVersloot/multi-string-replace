module.exports = function(str,replacementList) {
	if(Array.isArray(replacementList)) {
		for(var i = 0; i < replacementList.length; i++) {
			var obj = replacementList[i];
			if(typeof obj === 'object' && obj.substr && obj.to) {
				str = str.replace(obj.substr,obj.to);
			}
		}
		return str;
	}
	else {
		return false;
	}
}