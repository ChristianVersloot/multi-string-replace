# multi-string-replace #
## Installation instructions ##
Run `npm install multi-string-replace`
## Usage ##
An example:

```javascript
var multiStringReplace = require('multi-string-replace'),
	str = 'The quick brown fox jumps over the lazy dog',
	replacementList = [
		{
			substr: 'fox',
			to: 'bird'
		},
		{
			substr: 'brown',
			to: 'yellow'
		}
	];

var mst = multiStringReplace(str,replacementList);

console.log(mst); // The quick yellow bird jumps over the lazy dog
```